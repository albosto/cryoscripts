# import stuff #
import os
import time
from laser_control import laser_control
from date_time import date_time

laserCtrl = laser_control()
laserCtrl.attenuator.read_timeout(3000)

# variables #
AvgTime = [0.020 , 0.050 , 0.100 , 0.200 , 0.500 , 1.00] # in seconds
sleepTime = 2  # in seconds
filePath = 'nonlinearity' # folder for saving logs

# initial settings #
laserCtrl.powerMeter.pm_set_Rauto(laserCtrl.slotPowerMeter,0) # stops auto-ranging on PM
msg = laserCtrl.powerMeter.pm_is_Rauto(laserCtrl.slotPowerMeter) # check to see if auto-ranging is stopped
print 'Auto-ranging status: ' + str(msg)
msg = laserCtrl.powerMeter.pm_set_cont_mode(laserCtrl.slotPowerMeter,0) # stops continuous measurement mode
print 'Continuous mode status: ' + str(msg)

# file handling #
logfile = open(os.path.join(filePath,date_time('dateTime')+'_OFPM_NL.txt'),'w')

# start of code #
for w in range(0,6):
	Avg  = AvgTime[w]
	msg = laserCtrl.powerMeter.set_avg_time(laserCtrl.slotPowerMeter,Avg) # set and record averaging time
	print 'Averaging over ' + str(msg) + 's'
	msg = 'Averaging over ' + str(msg) + 's' + '\n'
	logfile.write(str(msg))
	logfile.flush()
	for x in range(0,2):
		laserCtrl.set_switch_port('A',2) # diverts A
		laserCtrl.set_switch_port('B',2) # diverts B
		u=x*10
		msg = laserCtrl.powerMeter.pm_set_range(laserCtrl.slotPowerMeter,'%f'%u) # sets range and reads the range
		print 'Range is currently set to ' + str(msg) + 'dB'
		msg = 'Range setting: ' + str(msg) + 'dB' + '\n'
		logfile.write(str(msg))
		logfile.flush()
		msg = laserCtrl.powerMeter.pm_zero_offset(laserCtrl.slotPowerMeter) # zeros for given range
		print 'Return code from machine for zeroing process: ' + str(msg) 
 		BGND = laserCtrl.read_power() # measures background
		laserCtrl.set_switch_port('A',1) # sends through A
		laserCtrl.set_switch_port('B',2) # diverts B
		msg = 'Switch A on \n'
		for y1 in range(0,10):
	 		A = laserCtrl.read_power()
			msg += str(time.time()) + '\t' +  str(A) + '\n'
		print msg
		logfile.write(str(msg))
		logfile.flush()
		msg = 'Switch AB on \n'
		laserCtrl.set_switch_port('A',1) # sends through A
		laserCtrl.set_switch_port('B',1) # sends through B
		for y2 in range(0,10):
			AB = laserCtrl.read_power()
			msg += str(time.time()) + '\t' + str(AB) + '\n'
		print msg
		logfile.write(str(msg))
		logfile.flush()
		msg = 'Switch B on \n'
		laserCtrl.set_switch_port('A',2) # diverts A
		laserCtrl.set_switch_port('B',1) # sends thorugh B
		for y3 in range(0,100):
			B = laserCtrl.read_power()
			time.sleep(Avg) 
			msg += str(time.time()) + '\t' + str(B) + '\n'
		print msg
		logfile.write(str(msg))
		logfile.flush()
	
logfile.close()
