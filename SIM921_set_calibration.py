#!/usr/bin/env python

import sys,os
import struct
import time
import string
import signal

import Gpib_prologix_nt_ver2 as Gpib
serialport = serial.Serial('/dev/ttyUSB0',9600,timeout=0.5)
curve = 21;
filename = 'Diodecal3.txt';

msgout = 'CRVHDR %d,%s,1,2,320,1'%(curve,filename)
#calfile = open('RuO2mean.txt')
calfile = open('Diodecal3.txt')
print msgout
idx = 1;
for line in calfile:
  vals = line.split()
  if vals[0].replace('.','',1).isdigit(): #This logic skips header if present  
    res = float(vals[1])
    temp = float(vals[0])
    s = 'CRVPT %d,%d1,%.3f,%.2f'%(curve,idx,res,temp)
    print s+'\n'
    idx = idx+1 
    #time.sleep(1)

calfile.close()
print 'Goodbye'



