#!/usr/bin/env python

import sys,os
import struct
import time
import string
import signal
import serial
import Gpib_prologix_nt_ver2 as Gpib
serialport = serial.Serial('/dev/ttyUSB0',9600,timeout=0.5)
ls = Gpib.Gpib(12,serialport)

msgout = 'CRVHDR? 23'
ls.write(msgout)
print ls.read(100)

ch = 'A'
msgout = 'INCRV %s, 23'%ch
print msgout
ls.write(msgout)

msgout = 'INCRV? %s'%ch
print msgout
ls.write(msgout)
msgin = ls.read(100)
print repr(msgin)

ls.write('KRDG? %s'%ch)
msgin = ls.read(100)
print repr(msgin)
ls.write('SRDG? %s'%ch)
msgin = ls.read(100)
print repr(msgin)


ls.loc()

