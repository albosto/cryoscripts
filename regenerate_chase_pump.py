########################
# Imports and settings #
########################
import sys
import time
from usb31xx import usb31xx
print 'import finish'
class regenerate_chase():
    
    def __init__(self, interval=1, average = 5, printOutput=True):
        self.pumpHeaterOn = False
        self.switchHeaterOn = False
        self.heatSwitchHot = False
        self.USB3103=usb31xx()
        self.get_settings()
        
    
    def get_settings(self):
        self.config = dict() #define config as a dictionary
        self.config['coldPlate_coldTemp'] = 1 #cold plate goes down below this
        self.config['coldPlate_regenTemp'] = 1.5 #start regeneration when cold plate raises to this temp
        self.config['coldPlate_switchTemp'] = 4 #start regeneration when cold plate raises to this temp
        self.config['coldPlate_firstCool_flagTemp'] = 10 #start regeneration when cold plate raises to this temp
        self.config['coldPlate_firstCool_readyTemp'] = 4 #start regeneration when cold plate raises to this temp
        self.config['heatPump_highTemp'] = 65 #maximum temp to drive the heat pump
        self.config['heatPump_lowTemp'] = 60 #temp to start driving the heat pump again
        self.config['heatPump_firstCool_highTemp'] = 23 #maximum temp to drive the heat pump
        self.config['heatPump_firstCool_lowTemp'] = 20 #temp to start driving the heat pump again
        self.config['heatSwitch_highTemp'] = 9 #maximum temp for heat switch
        self.config['heatSwitch_lowTemp'] = 8 #maximum temp for heat switch
        self.config['heatSwitch_regenTemp'] = 5   #temperateure heat switch is to be under before regen
        self.config['heatSwitch_firstCool_regenTemp'] = 5 #temperateure heat switch is to be under before regen
        
        self.tempFileName = 'templog_chase.txt'
        self.logFile='cyclelog.txt'
        
	self.T = dict()
        self.T['coldPlate'] = 0.0
        self.T['heatPump'] = 0.0
        self.T['heatSwitch'] = 0.0

	self.lastmsg = ''

    def print_once(self,msg):
        #does not print the same message twice in a row (same message as last call)
        if not msg == self.lastmsg:
            print(msg)
            self.lastmsg=msg
        else:
            sys.stdout.write('.')
            sys.stdout.flush()
        
    def getTemperature(self):  ###temperature logger needs to be edited to output to another file with specific formatting
        #pull temperatures from file
        success=False
        while not success:
            tempFile_chase = open(self.tempFileName,'r')
            temp=tempFile_chase.read()
            tempFile_chase.close()
            temp=temp.split()
            T=dict()
            try:    
                T['coldPlate'] = float(temp[7])
                T['heatPump'] = float(temp[4])
                T['heatSwitch'] = float(temp[5])
                self.T=T
                success = True
            except:
                #error, wait and try again
                time.sleep(1)  
        return T
    
    def cmd_heater(self,heater):
        if heater == 'off':
            #turn all heaters off (USB3103 digital port all 0)
            #send 00 to heater array   
            self.USB3103.USB31xx_Dout(0) 
            print 'heaters off'
            self.pumpHeaterOn = False
            self.switchHeaterOn = False
        elif heater == 'pump':
            #turn pump heater on (USB3103 digital port 0 on)
            #send 00 to heater array
            self.USB3103.USB31xx_Dout(1) 
            self.pumpHeaterOn = True
            self.switchHeaterOn = False
            print 'pump on'
        elif heater == 'switch':
            #turn switch heater on (USB3103 digital port 1 on)
            #send 00 to heater array
            self.USB3103.USB31xx_Dout(2) 
            self.pumpHeaterOn = False
            self.switchHeaterOn = True
            print 'switch on'
        else:
            return 'err'
        
    #####################
    # Cycle Segments    #
    #####################
    def first_cool_chase(self):
        done = False
        config=self.config
        print 'Checking if cryostat is in first cooldown stage'
        while not done:
            T=self.getTemperature()
            if (T['coldPlate']>float(config['coldPlate_firstCool_flagTemp'])) or (T['heatSwitch'] > float(config['heatSwitch_firstCool_regenTemp'])):
                self.print_once('Cryostat in first cooldown stage. Waiting for Cryostat to cool.')            
                if T['heatPump'] > float(config['heatPump_firstCool_highTemp']) and self.pumpHeaterOn:
                    print 'Heat pump too hot >23k, turning heaters off' 
                    print T
                    self.cmd_heater('off')
                elif T['heatPump'] < float(config['heatPump_firstCool_lowTemp']) and not self.pumpHeaterOn:
                    print 'Heat pump cold <20k, turning pump heater on' 
                    print T
                    self.cmd_heater('pump')
            else:
                print 'pump past first cooldown stage'
                done=True
            time.sleep(5)
    def waiting_chase(self):
        #check if at cold operating temperature;
        #   wait till time to cool down again\
        T=self.getTemperature()
        config=self.config
        self.cmd_heater('switch')
        while T['coldPlate']<float(config['coldPlate_regenTemp']):##########  change 
            self.cmd_heater('switch')
            self.print_once('Waiting for Helium run out before regenerating.')
            time.sleep(60)
            T=self.getTemperature()
        self.cmd_heater('off')
        self.print_once(str('Heat switch off. Waiting for heat switch to drop below 4K.'))
        print T
        while  T['heatSwitch'] > float(config['heatSwitch_regenTemp']):
            time.sleep(30)
            T=self.getTemperature()    
        
    def recycle_chase(self):  
        #check if first cooldown
        #   wail till cold enough to start heat pump
        #if not firstcool or below firstcool temp
        #   turn on heat pump
        #if above heat pump high 
        #   turn off heat pump
        #monitor heat pump an col temp
        #   cycle heat pump as needed
            # turn heaters off
        self.cmd_heater('off')
        config=self.config
        done = False
        heatSwitchHotCount = 0
        while not done:
            T=self.getTemperature()
            if T['heatSwitch'] > float(config['heatSwitch_highTemp']):
                self.heatSwitchHot = True
                print 'Heat switch too hot >9K , waitin for cooldown' 
                print T
                if self.pumpHeaterOn:
                    heatSwitchHotCount = heatSwitchHotCount + 1
                    print 'turning pump heater off.' 
                    print T
                    print heatSwitchHotCount                    
                    self.cmd_heater('off')
                while self.heatSwitchHot:
                    T=self.getTemperature()
                    if T['heatSwitch'] < float(config['heatSwitch_lowTemp']):
                        print 'Heat switch cooled' 
                        print T
                        self.heatSwitchHot = False
                    time.sleep(5)
                        
            if T['heatPump'] > float(config['heatPump_highTemp']) and self.pumpHeaterOn:
                print 'Heat pump too hot >55k, turning heaters off' 
                print T
                self.cmd_heater('off')
            elif T['heatPump'] < float(config['heatPump_lowTemp']) and not self.pumpHeaterOn:
                print 'Heat pump cold <50k, turning pump heater on'  
                print T
                self.cmd_heater('pump')
                
            if T['coldPlate'] < float(config['coldPlate_switchTemp']) and not self.pumpHeaterOn:
                done = True
            time.sleep(5)
        
        
    def cooldown_chase(self):    
        #when col condition met
        #   Turn on swich
        #   wait for cold operating temerature
        #restart loop

        print 'Cooling down cold plate'    
        print 'Helium heatswitch on'
        self.cmd_heater('switch')
        T=self.getTemperature()
        config=self.config
        print T
        while T['coldPlate']>float(config['coldPlate_coldTemp']):
            T=self.getTemperature()
            self.print_once('Waiting for helium to cool before next stage.')
            time.sleep(5)
        
    def regenerate(self,step=0):
        temprun=0
        tempRange=[70,75,60.65]
        logFile_chase = open(self.logFile,'a')
        print 'Excecuting first cool script'
        if step == -1:
            self.first_cool_chase()
            step=0
        while True:
            if step == 0:
                print 'Executing "waiting" script.'
                logFile_chase.write('%f\tWaiting\n'%(time.time()))
                self.waiting_chase()
                step=1
            if step == 1:
                print 'Executing "recycle" script.'
                self.config['heatPump_highTemp'] = tempRange[temprun] #maximum temp to drive the heat pump
                self.config['heatPump_lowTemp'] = tempRange[temprun]-5  #temp to start driving the heat pump again
                temprun+=1
                logFile_chase.write('%f\tRecycle\n'%(time.time()))
                self.recycle_chase()
                step=2
            if step == 2:
                print 'Executing "cooldown" script.'
                logFile_chase.write('%f\tCooldown\n'%(time.time()))
                self.cooldown_chase()
                step=0
    

#####################
# Main Script       #
#####################

if __name__ == '__main__':
    regen=regenerate_chase()
    step=int(raw_input('starting position, -1 = first, 0 = waiting, 1 = recycle, 2 = cooldown: '))
    regen.regenerate(step)
