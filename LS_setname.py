#!/usr/bin/env python

import sys,os
import struct
import time
import string
import signal
import math
import serial
import Gpib_prologix_nt_ver2 as Gpib
serialport = serial.Serial('/dev/ttyUSB2',9600,timeout=0.5)
ls = Gpib.Gpib(12,serialport)
#filename = 'Diodecal3.txt';
filename = 'FBdiode.txt';
curve = 23;
#filename = 'RuO2mean.txt';

#msgout = 'CRVHDR %d,%s,1,2,320,1'%(curve,filename)
msgout = 'CRVHDR %d,%s,1,4,300,1'%(curve,filename)
#calfile = open('RuO2mean.txt')
calfile = open(filename)

print msgout
ls.write(msgout)
ls.write('CRVHDR? %d'%curve)
msgin = ls.read(100)
print repr(msgin)
idx = 1;
for line in calfile:
  vals = line.split()
  if vals[0].replace('.','',1).isdigit(): #This logic skips header if present  
    res = float(vals[1])
#    res = math.log10(res) 
    temp = float(vals[0])
    if True:
      s = 'CRVPT %d,%d,%f,%f'%(curve,idx,res,temp)
      ls.write(s)
      print s
    #time.sleep(1)
    else:
      s = 'CRVPT? %d,%d'%(curve,idx)
      ls.write(s)
      #print s
      msgin = ls.read(100)
      print repr(msgin)
      print '%f \t %f'%(res,temp)
    idx = idx+1 
    #time.sleep(1)

calfile.close()
print 'Goodbye'



