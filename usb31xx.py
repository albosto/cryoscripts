# -*- coding: utf-8 -*-
#
# 
"""
Demonstrates how items may trigger callbacks when activated
"""

###############################################################################
import hid
import time
import struct

class usb31xx:
    
    def __init__(self, interval=1, average = 5, printOutput=False):
        self.hex_codes()
        self.printCommands = printOutput  #allow printing of messages
        self.printStatus = printOutput
        self.print_msgs('s',"Opening device")
        try:
            self.HIDid = hid.device(0x9db, 0x9c)     #open device for further commands
        except IOError, ex:
            try:
                self.HIDid = hid.device(0x9db, 0x9d)     #open device for further commands
            except IOError, ex:
                print ex
                print "You probably don't have the hard coded test hid. Update the hid.device line"
                print "in this script with one from the enumeration list output above and try again."
            
        self.print_msgs('s',"Manufacturer: %s" % self.HIDid.get_manufacturer_string()) #obtain device information
        self.print_msgs('s',"Product: %s" % self.HIDid.get_product_string())           #obtain device information
        self.print_msgs('s',"Serial No: %s" % self.HIDid.get_serial_number_string())   #obtain device information
        self.USB31xx_readMemoryFlash()     #get calibration fpor analouge outputs
    
    def hex_codes(self):
        self.hCode=dict()
        self.hCode['DIO_DIR_OUT']   = 0x00   
        
        self.hCode['BLINK']         = 0x40
        self.hCode['DCONFIG']       = 0x01     #Configure digital port
        self.hCode['DCONFIG_BIT']   = 0x02     #Configure individual digital port bits
        self.hCode['DIN']           = 0x03     #Read digital port
        self.hCode['DOUT']          = 0x04     #Write digital port
        self.hCode['DBIT_IN']       = 0x05     #Read digital port bit
        self.hCode['DBIT_OUT']      = 0x06     #Write digital port bit
        
        self.hCode['AOUT']          = 0x14     #Write analog output channel
        self.hCode['AOUT_SYNC']     = 0x15     #Synchronously update outputs
        self.hCode['AOUT_CONFIG']   = 0x1C     #Configure analog output channel
        self.hCode['MEM_READ']      = 0x30     # Read Memory

    def print_msgs(self,msgType,msg):
        if self.printCommands and msgType=='c':
            print msg
        if self.printStatus and msgType=='s':
            print msg
        
    def USB31xx_blink(self,nBlinks):
        self.print_msgs('c','blink %r times' %nBlinks)
        self.HIDid.write([self.hCode['BLINK'], nBlinks])
    
    def USB31xx_Dout(self, value):
        self.print_msgs('c','writing digital port: %r' %value)
        self.HIDid.write([self.hCode['DCONFIG'], self.hCode['DIO_DIR_OUT']])
        self.HIDid.write([self.hCode['DOUT'], value])
        
    def USB31xx_Aout(self, port, aVolt):
	port = int(float(port))
        self.print_msgs('c','set voltage on port %r to %r V' %(port, aVolt))
        aVolt=int(aVolt/10*255*255)            #convert voltage to range
        aVolt = int(self.slope[port]*aVolt + self.offset[port])  #apply calibration
        highBit,lowBit = divmod(aVolt,255)     #split into 2 bits
        self.HIDid.write([self.hCode['AOUT_CONFIG'], port, 0]) #configure port for output
        self.HIDid.write([self.hCode['AOUT'], port, lowBit, highBit, 0])   #set voltage
    
    def USB31xx_Aout_all(self, aVolt):
        for port in range(0,8):
            self.USB31xx_Aout(port, aVolt)
    
    def msgScrub(self,msg):
        msg = msg.replace('[','')
        msg = msg.replace(' ','')
        msg = msg.replace(']','')
        msg = msg.replace('\'','')
        msg = msg.replace(',',' ')
        return msg
    
    def USB31xx_readMemoryFlash(self):
        ### reads and returns the slope and offset calibration data 
        ### for the USB3103/USB3104  analouge outputs
        self.print_msgs('s','loading analoge data from device')
        l=[]    #initialise empty aray
        stAddress = 0x100                           #start location of calibration data
        for x in range(0,8):                        #for each analouge output
            adr = stAddress+x*0x10                  #address to read from
            highBit,lowBit = divmod(int(adr),255)   # device can only read small bits at a time, so divide into 2 bits
            # infom device that you want to read from memory, at a certain address, and so many bytes
            self.HIDid.write([self.hCode['MEM_READ'] , lowBit, highBit, 0x0, 16])  
            time.sleep(0.1)         #wait 0.1 seconds
            msg=self.msgScrub(str(self.HIDid.read(16))) # read from memort and turn to string, remove characters from msg, replace(,) with space
            for y in msg.split():   # split message into chuncks, then for each chunk
                try:                # attept to
                    l.append(y)     # append chunk into an array
                except ValueError:  # if append operation fails
                    pass            # do  nothing 
                
        self.slope=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]     #initialise slope array
        self.offset=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]    #initialise ofset array
        for x in range (0,8):       # Once for each output
            adr = x*16              # there are 16 bytes of information for each output
            for y in range(0,5,4):  # y = 0,4. 0 for the adress of slopes, 4 for the addess of offsets
                floatPack=''            # create empty string
                for z in range(0,4):    # read through 4 consecutive adresses
                    a=bytes(chr(int(l[adr+y+z]))) #turn string > integer > character > byte
                    floatPack=floatPack+a       #add byte to byte string
                #unpack a 4 byte string into a tupple, convert to string then remove characters         
                floatStr=str(struct.unpack('f',floatPack)).strip(',()')
                if y==0:            #add float to slope configuratiom
                    self.slope[x]= float(floatStr)
                elif y==4:          #add float to offset configuratiom
                    self.offset[x]= float(floatStr)    
        self.print_msgs('s','loaded slopes and offsets')
        
    def close(self):
        self.print_msgs('s',"Closing device")
        self.HIDid.close()
    

##############################################################################    

if __name__ == '__main__':
    usb3100=usb31xx()    


    def open_gui():
        SHOW = True # Show test in GUI-based test launcher
        from guidata.dataset.datatypes import DataSet
        from guidata.dataset.dataitems import (TextItem, ButtonItem, FloatItem, StringItem)
        digital = 0
        
        def set_av_port(self, port, voltage):
            self.Log = 'Setting channel %r to %r Volts' %(port,voltage) + '\n' + self.Log
            usb3100.USB31xx_Aout(usb3100.HIDid, port, voltage)
                
        def set_digital(self, bit):
            global digital
            digital=digital+bit
            usb3100.USB31xx_Dout(usb3100.HIDid, digital)
                
        class usb31xx_gui(DataSet):
                
            def set_av_0(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge0 = str(self.inputAnalouge0) + 'V'
                port = 0
                voltage=self.inputAnalouge0
                set_av_port(self, port, voltage)
                
            def set_av_1(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge1 = str(self.inputAnalouge1) + 'V'
                port = 1
                voltage=self.inputAnalouge1
                set_av_port(self, port, voltage)
        
            def set_av_2(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge2 = str(self.inputAnalouge2) + 'V'
                port = 2
                voltage=self.inputAnalouge2
                set_av_port(self, port, voltage)
                
            def set_av_3(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge3 = str(self.inputAnalouge3) + 'V'
                port = 3
                voltage=self.inputAnalouge3
                set_av_port(self, port, voltage)
                
            def set_av_4(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge4 = str(self.inputAnalouge4) + 'V'
                port = 4
                voltage=self.inputAnalouge4
                set_av_port(self, port, voltage)
                
            def set_av_5(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge5 = str(self.inputAnalouge5) + 'V'
                port = 5
                voltage=self.inputAnalouge5
                set_av_port(self, port, voltage)
                
            def set_av_6(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge6 = str(self.inputAnalouge6) + 'V'
                port = 6
                voltage=self.inputAnalouge6
                set_av_port(self, port, voltage)
                
            def set_av_7(self,notUsed3,notUsed,notUsed2):
                self.currentAnalouge7 = str(self.inputAnalouge7) + 'V'
                port = 7
                voltage=self.inputAnalouge7
                set_av_port(self, port, voltage)
                
            def set_d0_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital0 == "OFF":
                    self.currentDigital0 = "ON"
                    bit = 1 
                    set_digital(self, bit)        
            def set_d0_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital0 == "ON":
                    self.currentDigital0 = "OFF"
                    bit = -1 
                    set_digital(self, bit)
                
            def set_d1_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital1 == "OFF":
                    self.currentDigital1 = "ON"
                    bit = 2
                    set_digital(self, bit)        
            def set_d1_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital2 == "ON":
                    self.currentDigital2 = "OFF"
                    bit = -2
                    set_digital(self, bit)
                    
            def set_d2_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital2 == "OFF":
                    self.currentDigital2 = "ON"
                    bit = 4
                    set_digital(self, bit)        
            def set_d2_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital2 == "ON":
                    self.currentDigital2 = "OFF"
                    bit = -4
                    set_digital(self, bit)
                    
            def set_d3_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital3 == "OFF":
                    self.currentDigital3 = "ON"
                    bit = 8
                    set_digital(self, bit)        
            def set_d3_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital3 == "ON":
                    self.currentDigital3 = "OFF"
                    bit = -8
                    set_digital(self, bit)
                    
            def set_d4_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital4 == "OFF":
                    self.currentDigital4 = "ON"
                    bit = 16
                    set_digital(self, bit)        
            def set_d4_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital4 == "ON":
                    self.currentDigital4 = "OFF"
                    bit = -16
                    set_digital(self, bit)
                    
            def set_d5_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital5 == "OFF":
                    self.currentDigital5 = "ON"
                    bit = 32
                    set_digital(self, bit)        
            def set_d5_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital5 == "ON":
                    self.currentDigital5 = "OFF"
                    bit = -32
                    set_digital(self, bit)
                    
            def set_d6_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital6 == "OFF":
                    self.currentDigital6 = "ON"
                    bit = 64
                    set_digital(self, bit)        
            def set_d6_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital6 == "ON":
                    self.currentDigital6 = "OFF"
                    bit = -64
                    set_digital(self, bit)
                             
            def set_d7_on(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital7 == "OFF":
                    self.currentDigital7 = "ON"
                    bit = 128
                    set_digital(self, bit)        
            def set_d7_off(self,notUsed3,notUsed,notUsed2):
                if self.currentDigital7 == "ON":
                    self.currentDigital7 = "OFF"
                    bit = -128
                    set_digital(self, bit)
                    
            
                
            inputAnalouge0 = FloatItem("A1 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge0 = ButtonItem("Set", callback=set_av_0).set_pos(col=1)
            currentAnalouge0 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge1 = FloatItem("A1 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge1 = ButtonItem("Set", callback=set_av_1).set_pos(col=1)
            currentAnalouge1 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge2 = FloatItem("A2 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge2 = ButtonItem("Set", callback=set_av_2).set_pos(col=1)
            currentAnalouge2 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge3 = FloatItem("A3 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge3 = ButtonItem("Set", callback=set_av_3).set_pos(col=1)
            currentAnalouge3 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge4 = FloatItem("A4 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge4 = ButtonItem("Set", callback=set_av_4).set_pos(col=1)
            currentAnalouge4 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge5 = FloatItem("A5 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge5 = ButtonItem("Set", callback=set_av_5).set_pos(col=1)
            currentAnalouge5 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge6 = FloatItem("A6 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge6 = ButtonItem("Set", callback=set_av_6).set_pos(col=1)
            currentAnalouge6 = StringItem("C Voltage", default=0).set_pos(col=2)
            inputAnalouge7 = FloatItem("A7 Voltage", default=0, min=0, max=10, step=0.01, slider=True).set_pos(col=0)
            butonAnalouge7 = ButtonItem("Set", callback=set_av_7).set_pos(col=1)
            currentAnalouge7 = StringItem("C Voltage", default=0).set_pos(col=2)
            
            butonDigital0on = ButtonItem("D0 ON", callback=set_d0_on).set_pos(col=0)
            butonDigital0off = ButtonItem("D0 OFF", callback=set_d0_off).set_pos(col=1)
            currentDigital0 = StringItem("Digital 0", default = 'OFF').set_pos(col=2)
            butonDigital1on = ButtonItem("D1 ON", callback=set_d1_on).set_pos(col=0)
            butonDigital1off = ButtonItem("D1 OFF", callback=set_d1_off).set_pos(col=1)
            currentDigital1 = StringItem("Digital 1", default = 'OFF').set_pos(col=2)
            butonDigital2on = ButtonItem("D2 ON", callback=set_d2_on).set_pos(col=0)
            butonDigital2off = ButtonItem("D2 OFF", callback=set_d2_off).set_pos(col=1)
            currentDigital2 = StringItem("Digital 2", default = 'OFF').set_pos(col=2)
            butonDigital3on = ButtonItem("D3 ON", callback=set_d3_on).set_pos(col=0)
            butonDigital3off = ButtonItem("D3 OFF", callback=set_d3_off).set_pos(col=1)
            currentDigital3 = StringItem("Digital 3", default = 'OFF').set_pos(col=2)
            butonDigital4on = ButtonItem("D4 ON", callback=set_d4_on).set_pos(col=0)
            butonDigital4off = ButtonItem("D4 OFF", callback=set_d4_off).set_pos(col=1)
            currentDigital4 = StringItem("Digital 4", default = 'OFF').set_pos(col=2)
            butonDigital5on = ButtonItem("D5 ON", callback=set_d5_on).set_pos(col=0)
            butonDigital5off = ButtonItem("D5 OFF", callback=set_d5_off).set_pos(col=1)
            currentDigital5 = StringItem("Digital 5", default = 'OFF').set_pos(col=2)
            butonDigital6on = ButtonItem("D6 ON", callback=set_d6_on).set_pos(col=0)
            butonDigital6off = ButtonItem("D6 OFF", callback=set_d6_off).set_pos(col=1)
            currentDigital6 = StringItem("Digital 6", default = 'OFF').set_pos(col=2)
            butonDigital7on = ButtonItem("D7 ON", callback=set_d7_on).set_pos(col=0)
            butonDigital7off = ButtonItem("D7 OFF", callback=set_d7_off).set_pos(col=1)
            currentDigital7 = StringItem("Digital 7", default = 'OFF').set_pos(col=2)
            
            Log = TextItem("Log")
    
    ###########################
    # start application
    ####################### 
        # Create QApplication
        import guidata
        _app = guidata.qapplication()
        
        gui = usb31xx_gui()
        gui.edit()
        usb3100.close()
    ################################################
    # end of gui stuff
    #################################################
    runCode=True
    while runCode:
        mode= raw_input('Select mode;\n\t Analouge: a\n\t Analouge all ports: a_all\n\t Digital: d\n\t Graphical user injterface: gui\n\t Quit: q \nInput: ').lower()
        if mode == 'q':
            runCode = False
            break
        elif mode == 'gui':
            open_gui()
            runcode = False
            break
        elif mode == 'd':
            done = False
            while not done:
                ports = str(raw_input('Seclect output ports(integer: 0 to 256)(q=quit): '))
                if ports == 'q':
                    done = True
                    break
                else:
		    ports = int(ports)
                    usb3100.USB31xx_Dout(ports)
        elif mode == 'a_all':
	    done = False
            while not done:
                voltage=raw_input('set voltage(decimal 0 to 10)(q=quit): ')
                if voltage == 'q':
                    done = True
                    break
                else:
                    voltage=float(voltage)
                    usb3100.USB31xx_Aout_all(voltage)
                    usb3100.USB31xx_blink(1)
        elif mode == 'a':
	    done = False
            while not done:
                port=raw_input('set port (integer 0 to 7)(q=quit): ')
                voltage=raw_input('set voltage(decimal 0 to 10)(q=quit): ')
                if voltage == 'q' or port == 'q':
                    done = True
                    break
                else:
                    voltage=float(voltage)
                    usb3100.USB31xx_Aout(port,voltage)
                    usb3100.USB31xx_blink(1)
    usb3100.close()
        
