# -*- coding: utf-8 -*-
"""
Created on Tue Mar  4 10:57:12 2014

@author: allen
"""
def date_time(output):
    import time
    lTime=time.localtime()
    if lTime.tm_mon <10:
        month ='0'+str(lTime.tm_mon)
    else:
        month =str(lTime.tm_mon)
    if lTime.tm_mday <10:
        day ='0'+str(lTime.tm_mday)
    else:
        day =str(lTime.tm_mday)
    if lTime.tm_hour <10:
        hour ='0'+str(lTime.tm_hour)
    else:
        hour =str(lTime.tm_hour)
    if lTime.tm_min <10:
        minuite ='0'+str(lTime.tm_min)
    else:
        minuite =str(lTime.tm_min)
            
    strDate=str(lTime.tm_year)+month+day
    strTime = hour+minuite
    strDateTime = strDate+'_'+strTime
    if output in ('0', 'date'):
        return strDate
    elif output in ('1', 'time'):
        return strTime
    elif output in ('0', 'dateTime'):
        return strDateTime
        
        