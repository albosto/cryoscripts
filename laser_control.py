# -*- coding: utf-8 -*-
"""
Created on Mon Mar  3 15:37:24 2014

@author: allen
"""
from ag8166_allen import dev
import serial
import time
class laser_control:
    def __init__(self,serialport='/dev/ttyUSB0'):
        addrPowerMeter      = 9
        addrOpticalSwitch   = 10
        addrAttenuator      = 10
        addrLaser           = 21
        addrPolarisation    = 5
        self.slotAttenuator1     = 9
        self.slotAttenuator2     = 12
        self.slotAttenuator3     = 15
        self.slotPowerMeter      = 2
        self.slotOpticalSwitch   = 7
        serialPort = serial.Serial(serialport,9600,timeout=1);
        
        self.powerMeter      =dev(addrPowerMeter, serialPort)
        self.opticalSwitch   =dev(addrOpticalSwitch, serialPort)
        self.attenuator      =dev(addrAttenuator, serialPort)
        self.laser           =dev(addrLaser, serialPort)
        self.polarisation    =dev(addrPolarisation, serialPort)
        
    
    def get_info(self):
        print 'aligent 8163A - contains power meter'
        print self.powerMeter.info()
        print '81533B power meter'
        print self.powerMeter.info_slot(self.slotPowerMeter )
        print 'aligent 8166A - contains switch and attenuators'
        print self.attenuator.info()
        print '81592A - Optical Switch'
        print self.opticalSwitch.info_slot(self.slotOpticalSwitch)
        print '81567A - Attenuator 1'        
        print self.attenuator.info_slot(self.slotAttenuator1)
        print '81567A - Attenuator 2' 
        print self.attenuator.info_slot(self.slotAttenuator2)
        print '81567A - Attenuator 3' 
        print self.attenuator.info_slot(self.slotAttenuator3)
        
    def get_attenuation(self):
        attenValue1 = self.attenuator.get_att(self.slotAttenuator1)
        attenValue2 = self.attenuator.get_att(self.slotAttenuator2)
        attenValue3 = self.attenuator.get_att(self.slotAttenuator3)
        attenWave1  = self.attenuator.get_lambda(self.slotAttenuator1)
        attenWave2  = self.attenuator.get_lambda(self.slotAttenuator2)
        attenWave3  = self.attenuator.get_lambda(self.slotAttenuator3)
        print 'Attenuation 1 = %r DB' %attenValue1
        print 'at wavelength %r nm' %attenWave1
        print 'Attenuation 2 = %r' %attenValue2
        print 'at wavelength %r nm' %attenWave2
        print 'Attenuation 3 = %r' %attenValue3
        print 'at wavelength %r nm' %attenWave3
        
    def set_attenuation(self, value, idn):
        if idn == 0:
            self.attenuator.set_att(self.slotAttenuator1,value)
        elif idn ==1:
            self.attenuator.set_att(self.slotAttenuator2,value)
        elif idn ==2:            
            self.attenuator.set_att(self.slotAttenuator3,value)
        
    def set_attenuation_all(self, value):
        #value,vRem=divmod(value,3)
        self.attenuator.set_att(self.slotAttenuator1,value)
        self.attenuator.set_att(self.slotAttenuator2,value)
        self.attenuator.set_att(self.slotAttenuator3,value)
        
    def set_wavelength(self, wavel):
        self.attenuator.setlambda(self.slotAttenuator1,wavel)
        self.attenuator.setlambda(self.slotAttenuator2,wavel)
        self.attenuator.setlambda(self.slotAttenuator3,wavel)
        attenWave1  = self.attenuator.get_lambda(self.slotAttenuator1)
        attenWave2  = self.attenuator.get_lambda(self.slotAttenuator2)
        attenWave3  = self.attenuator.get_lambda(self.slotAttenuator3)
        print 'Attenuation 1 wavelength %r nm' %attenWave1
        print 'Attenuation 2 wavelength %r nm' %attenWave2
        print 'Attenuation 3 wavelength %r nm' %attenWave3
        
    def attenuator_on(self):
        self.attenuator.enable(self.slotAttenuator1)
        self.attenuator.enable(self.slotAttenuator2)
        self.attenuator.enable(self.slotAttenuator3)
        
    def attenuator_off(self):
        self.attenuator.disable(self.slotAttenuator1)
        self.attenuator.disable(self.slotAttenuator2)
        self.attenuator.disable(self.slotAttenuator3)
        
    def set_switch_port(self,inPort, outPort):
        if inPort.lower() == 'a':
            channel = 1
        elif inPort.lower() == 'b':
            channel = 2
        self.opticalSwitch.route(self.slotOpticalSwitch,channel,' %s,%s'%(str(inPort),str(outPort)))
        
    def read_power(self):
        return self.powerMeter.get_pow(self.slotPowerMeter)
        
if __name__ == "__main__":
    laserCtrl = laser_control()
    #laserCtrl.get_info()
    #laserCtrl.get_attenuation()
    #laserCtrl.set_wavelength(1550)
    #laserCtrl.set_attenuation(60)
    #laserCtrl.get_attenuation()
    #laserCtrl.attenuator_on()
    #time.sleep(5)
    #laserCtrl.attenuator_off()
    #laserCtrl.set_switch_port(2)
#    laserCtrl.opticalSwitch.meter.write('rout7:chan1 B,1')
#    print laserCtrl.opticalSwitch.get_route(laserCtrl.slotOpticalSwitch)
#    laserCtrl.set_switch_port(2)
#    print laserCtrl.opticalSwitch.get_route(laserCtrl.slotOpticalSwitch)
    done = False
    while not done:
        attenuation = int(float(raw_input('Attenuation setting: ')))
	laserCtrl.set_attenuation_all(attenuation)
       	print 'Power'
       	time.sleep(2)
       	print laserCtrl.read_power()

