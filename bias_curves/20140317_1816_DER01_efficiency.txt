Power @ 0 db 1.504078e-05
Power 1 @ 27 db 3.024321e-08
Power 2 @ 27 db 3.009428e-08
Power 3 @ 27 db 3.070297e-08

loss attnuator 1 = -26.9664247384
loss attnuator 2 = -26.9878640155
loss attnuator 3 = -26.9008997067
total attenuator loss = -80.8551884606

input power = 1.07651207187e-13
input power AR corrected = 1.11526650646e-13
input power AR + fiber loss correted = 1.06507124511e-13
input photons = 839408.48783
input photons AR corrected = 869627.193392
input photons AR + fiber loss correted = 830487.522292
