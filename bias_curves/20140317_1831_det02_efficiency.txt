Power @ 0 db 1.496018e-05
Power 1 @ 27 db 2.998852e-08
Power 2 @ 27 db 2.992895e-08
Power 3 @ 27 db 3.055399e-08

loss attnuator 1 = -26.9798178607
loss attnuator 2 = -26.9884533825
loss attnuator 3 = -26.8986888681
total attenuator loss = -80.8669601114

input power = 1.22529541337e-13
input power AR corrected = 1.26940604826e-13
input power AR + fiber loss correted = 1.26940604826e-13
input photons = 955422.04956
input photons AR corrected = 989817.243345
input photons AR + fiber loss correted = 989817.243345
