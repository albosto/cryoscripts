# import stuff #
import os
import time
from laser_control import laser_control
from date_time import date_time

laserCtrl = laser_control()


# variables #
AvgTime = 0.1 # in seconds
sleepTime = 2  # in seconds
filePath = 'nonlinearity' # folder for saving logs
measStep = 1;

# initial settings #
laserCtrl.powerMeter.pm_set_Rauto(laserCtrl.slotPowerMeter,0) # stops auto-ranging on PM
msg = laserCtrl.powerMeter.pm_is_Rauto(laserCtrl.slotPowerMeter) # check to see if auto-ranging is stopped
print 'Auto-ranging status: ' + str(msg)
msg = laserCtrl.powerMeter.pm_set_cont_mode(laserCtrl.slotPowerMeter,0) # stops continuous measurement mode
print 'Continuous mode status: ' + str(msg)

# file handling #
logfile = open(os.path.join(filePath,date_time('dateTime')+'_OFPM_NL.txt'),'w')

# start of code #
msg = laserCtrl.powerMeter.set_avg_time(laserCtrl.slotPowerMeter,AvgTime) # set and record averaging time
print 'Averaging over ' + str(msg) + 's'
msg = 'Averaging over ' + str(msg) + 's' + '\n'
logfile.write(str(msg))
logfile.flush()
lower = [0 , 0 , 0 , 7 , 17 , 27]
upper = [30 , 30 , 40 , 50 , 60 , 60]
for x in range(0,6):
    laserCtrl.set_switch_port('A',2) # diverts A
    laserCtrl.set_switch_port('B',2) # diverts B
    u=x*10
    msg = laserCtrl.powerMeter.pm_set_range(laserCtrl.slotPowerMeter,'%f'%u) # sets range and reads the range
    print 'Range is currently set to ' + str(msg) + 'dB'
    msg = 'Range setting: ' + str(msg) + 'dB' + '\n'
    logfile.write(str(msg))
    logfile.flush()
    time.sleep(sleepTime) # waits for power to settle
    msg = laserCtrl.powerMeter.pm_zero_offset(laserCtrl.slotPowerMeter) # zeros for given range
    print 'Return code from machine for zeroing process: ' + str(msg) 
    for y in range(0,int(30/measStep)+1):
        atten=lower[x]+y*measStep
        laserCtrl.set_attenuation(atten,0) # sets new power
        time.sleep(sleepTime) # waits for power change to settle
       # A = [0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0] # make array for A
       # AB = [0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0] # make array for AB
       # B = [0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0 , 0.0 , 0.0, 0.0] # make array for B
        A = [ 0.0 , 0.0 , 0.0 ]
	B = [ 0.0 , 0.0 ,0.0 ]
	AB = [ 0.0 ,0.0 , 0.0 ]
	laserCtrl.set_switch_port('B',2) # diverts B
        time.sleep(sleepTime)
        BGND = laserCtrl.read_power() # measures background
        for z in range(0,3):
            laserCtrl.set_switch_port('A',1) # sends through A
            laserCtrl.set_switch_port('B',2) # diverts B
            time.sleep(sleepTime) # waits for power change to settle
            A[z] = laserCtrl.read_power()
            time.sleep(AvgTime) # waits for an averaged  reading to finish
            laserCtrl.set_switch_port('A',1) # sends through A
            laserCtrl.set_switch_port('B',1) # sends through B
            time.sleep(sleepTime) # waits for power change to settle
            AB[z] = laserCtrl.read_power()
            time.sleep(AvgTime) # waits for an averaged reading to finish
            laserCtrl.set_switch_port('A',2) # diverts A
            laserCtrl.set_switch_port('B',1) # sends thorugh B
            time.sleep(sleepTime) # waits for power change to settle
            B[z] = laserCtrl.read_power()
        msg = str(float(y)*measStep) + '\t' + str(BGND)
        for w in range(0,3):
            for v in range(0,3):
                if w==0:
                    msg += '\t' +  str(A[v])
                if w==1:
                    msg += '\t' +  str(B[v])
                if w==2:
                    msg += '\t' +  str(AB[v])
        msg+='\n'
        print msg
        logfile.write(str(msg))
        logfile.flush()
logfile.close()
