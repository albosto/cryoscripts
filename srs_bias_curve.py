# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:20:02 2014

@author: allen
"""
###### IMPORTS ######
from laser_control import laser_control
from usb31xx import usb31xx
from sr400 import sr400
import SIM900serial # revise this
import time
from usbenumerator import USBEnumerator
import os
from date_time import date_time
import math

###### DEFINE VALUES ######
defVoltStart = .1
defVoltEnd =.7
defVoltStep = .01
defCountTime = 1

measureTime = 0.1 # in seconds
sleepTime = 2  # in seconds
atten = 27


###### FILE PATHS ######
filePath = 'bias_curves'
if not os.path.exists(filePath):
    os.makedirs(filePath)
Fname=raw_input('detector number: ')
fDark = open(os.path.join(filePath, date_time('dateTime') +'_'+ Fname + '_dark.txt'), 'w')
fLaser = open(os.path.join(filePath, date_time('dateTime') +'_'+ Fname + '_laser.txt'), 'w')
fEfficiency = open(os.path.join(filePath, date_time('dateTime') +'_'+ Fname + '_efficiency.txt'), 'w')

###### DETECT AND CONNECT EXTERNAL DEVICES ######
devices = {'Prologix':{'vendor':'0403','product':'6001'},
       'GenericSerial':{'vendor':'067b','product':'2303'}}
e = USBEnumerator(devices)
sim = SIM900serial.device(e.devices['GenericSerial']['port'])
sPort=e.devices['Prologix']['port']
counter=sr400(23,sPort)
laserCtrl = laser_control(sPort)
usb3103=usb31xx()

#########################
###### DEFINITIONS ######
#########################
def get_data():
    time.sleep(5)
    counter.counter_reset()
    counter.counter_start()
    time.sleep(countTime+1)
    countsa=counter.getcounts_a()
    countsb=counter.getcounts_b()
    sim.conn(1)
    sim.write('VOLT? 2')
    time.sleep(0.5)
    msg = sim.read()
    sim.write('VOLT? 1')
    time.sleep(0.5)
    msg2 = sim.read()
    sim.write('xyz')
    
    msgs = msg.strip("\r\n")
    msgs2 = msg2.strip("\r\n")
    msgs3 = countsa.strip("\r\n")
    msgs4 = countsb.strip("\r\n")
    msgs5 = msgs + "\t" + msgs2 + "\t" + msgs3 + "\t" + msgs4 
    return msgs5

###### USER INPUTS ######
Bias = raw_input('Bias curves (dark, laser, both) (default = both): ').lower()
if Bias in ('','both'):
    biasDark = True
    biasLaser = True
elif Bias == 'dark':
    biasDark = True
    biasLaser = False
elif Bias == 'laser':
    biasDark = False
    biasLaser = True
    
Vstart=raw_input('Start voltage (default = %s): ' %defVoltStart)
if Vstart == '':
    Vstart = defVoltStart
else:
    Vstart = float(Vstart)

Vend=raw_input('Finish voltage (default = %s): ' %defVoltEnd)
if Vend == '':
    Vend = defVoltEnd
else:
    Vend = float(Vend)

Vstep=raw_input('Step voltage (default = %s Volts): ' %defVoltStep)
if Vstep == '':
    Vstep = defVoltStep
else:
    Vstep = float(Vstep)

countTime=raw_input('Time to count (default = %s seconds): ' %defCountTime)
if countTime == '':
    countTime = defCountTime
else:
    countTime = float(countTime)
    



##########################
###### MAIN PROGRAM ######
##########################



laserCtrl.set_attenuation_all(atten) # sets new power

steps=int((Vend-Vstart)/Vstep+1)
usb3103.USB31xx_Aout_all(0) # Resets the state of the detector before run starts
time.sleep(2)
maxCounts=0
for x in range(0,steps,1):
    y=float(x*Vstep+Vstart)
    usb3103.USB31xx_Aout_all(y)
    time.sleep(2)
    if biasDark:
        laserCtrl.attenuator_off()
        msg = get_data()
        print msg + "\t *Dark* \n"
        fDark.write(msg)
        fDark.flush()
    if biasLaser:
        laserCtrl.attenuator_on()
        msg = get_data()
        print msg + "\t *Laser* \n"
        fLaser.write(msg)
        fLaser.flush()


fDark.close()
fLaser.close() 
fEfficiency.close()
usb3103.USB31xx_Aout_all(0) 
usb3103.close()
