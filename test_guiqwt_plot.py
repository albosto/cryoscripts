# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 11:51:00 2013

@author: qittlab
"""

# -*- coding: utf-8 -*-
#
# Copyright © 2009-2010 CEA
# Pierre Raybaut
# Licensed under the terms of the CECILL License
# (see guiqwt/__init__.py for details)

"""CurveDialog test"""

SHOW = True # Show test in GUI-based test launcher

from guidata.qt.QtGui import QFont

from guiqwt.plot import CurveDialog
from guiqwt.builder import make
from scales import DateTimeScaleEngine
from guidata.qt.compat import getsavefilename, getopenfilename
from guiqwt.config import _

def plot(*items):
    win = CurveDialog(edit=True, toolbar=True, wintitle="CurveDialog test",
                      options=dict(title="Title", xlabel="xlabel",
                                   ylabel="ylabel"))
    plot = win.get_plot()
    for item in items:
        plot.add_item(item)
    plot.set_axis_font("left", QFont("Courier"))
    win.get_itemlist_panel().show()
    plot.set_items_readonly(False)
#        plot = self.get_active_plot()
#        if plot is not None:
#            for axis,isTime in zip(plot.get_active_axes(), (xIsTime, yIsTime)):
#                if isTime:
#                    DateTimeScaleEngine.enableInAxis(plot, axis, rotation=-45)
#                else:
#                    DateTimeScaleEngine.disableInAxis(plot, axis)
#            plot.replot()
    axes = plot.get_active_axes()
    DateTimeScaleEngine.enableInAxis(plot, axes[0], rotation=-45)
   
    win.show()
    win.exec_()

def test():
    """Test"""
    # -- Create QApplication
    import guidata
    _app = guidata.qapplication()
    # --
    import numpy as np
    filename,_f = getopenfilename(None,_("Open"),'C:/Users/qittlab/Documents/Cooldown_data/','*' )
    print filename
    data = np.loadtxt(filename,skiprows=1)
    #data = np.genfromtxt(filename, skip_header=1)[1:100]
    plot(
         make.curve(data[:,0], data[:,2], color="b"),
         make.curve(data[:,0], data[:,3], color="g"),
         make.curve(data[:,0], data[:,4], color="r"),
         make.curve(data[:,0], data[:,1], color="y"),
         make.curve(data[:,0], data[:,6], color="c"),
         make.curve(data[:,0], data[:,7], color="k"),
         make.legend("TR")
#         make.marker(position=(5., .8), label_cb=lambda x, y: "A = %.2f" % x,
#                     markerstyle="|", movable=True)
         )

if __name__ == "__main__":
    test()
