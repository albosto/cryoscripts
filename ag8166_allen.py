# -*- coding: UTF-8 -*-
#!/usr/bin/env python
from Gpib_prologix_nt_ver2 import Gpib
import time
import ctypes
import string
class dev:
    
    def __init__(self,addr,serialport):
        """Connect to and reset an agilent 8166A"""
        meter = Gpib(addr,serialport)
        self.meter = meter
        #self.reset()
        #meter.write('ZERO:AUTO OFF'   
        
    def rd(self):     
          #reads message from device
        msg = self.meter.read();
        list1 = string.split(msg);
        return list1[0]
        
    def enable(self,slot):    #enable attenuator
        msg = 'OUTP%s:STAT ON'%(slot)
        self.meter.write(msg)

    def disable(self,slot):     #disable attenuator
        msg = 'OUTP%s:STAT OFF'%(slot)
        self.meter.write(msg)

    def get_stat(self,slot):
        try:
            msg = 'OUTP%s:STAT?'%(slot)
            self.meter.write(msg)
            reading = self.rd()
            return int(reading)
        except self.meter.error:
            return -1 
    def read_timeout(self,time):
        self.meter.timeout(str(time))
    def get_route(self,slot): #find which route the switch s using
        try:
            msg = 'ROUT%s:CHAN1?'%(slot)
            self.meter.write(msg)
            reading = self.rd()
            print reading
            msg = 'ROUT%s:CHAN2?'%(slot)
            self.meter.write(msg)
            reading = self.rd()
            list_switches = string.split(reading,';')
            switch = [];
            for count in range(len(list_switches)):
                temp = list_switches[count];
                temp = string.split(temp,',')
                switch.append(int(temp[1]));
            return switch 
        except self.meter.error:
            return -1 

    def route(self,slot,channel,port): #set optical switch
        msg = 'rout%s:chan%s%s'%(str(slot),str(channel),port)
        self.meter.write(msg)
        
    def set_att(self, slot, att):   #set attenuator attenuation
        """Set the attenuation"""
        msg = 'INP%s:ATT %s'%(str(slot),str(att))
        self.meter.write(msg)

    def set_lambda(self, slot, wav):    #set attenuation wavelength
        """Set the wavelength in nm"""
        msg = 'INP%s:WAV %sE-9'%(str(slot),str(wav))
        self.meter.write(msg)

    def set_pow_lambda(self, slot, wav):
        """Set the wavelength in nm"""
        msg = 'SENS%s:POW:WAV %sE-9'%(str(slot),str(wav))
        self.meter.write(msg)
 
    def set_source_lambda(self, slot, wav):
        """Set the wavelength in nm"""
        msg = 'SOUR%s:WAV %sE-9'%(str(slot),str(wav))
        self.meter.write(msg)

    def get_att(self, slot):    #get attenuation from attenuator
        """Get the attenuation"""
        try:
            msg = ':INP%s:ATT?'%(str(slot))
            print msg
            self.meter.write(msg)
            reading = self.rd()
            return float(reading)
        except self.meter.error:
            return None
    
    def get_lambda(self, slot): #get wavlength from the attenuator
        """Get the wavelength"""
        try:
            msg = ':INP%s:WAV?'%(str(slot))
            self.meter.write(msg)
            reading = self.rd()
            print reading
            return float(reading)
        except self.meter.error:
            return None

### POWER METER ###          
    def get_pow_lambda(self, slot):
        """Get the wavelength"""
        try:
            msg = ':SENS%s:POW:WAV?'%(str(slot))
            self.meter.write(msg)
            reading = self.rd()
            return float(reading)
        except self.meter.error:
            return None
      
    def get_source_lambda(self, slot):
        """Get the wavelength for laser"""
        try:
            msg = ':SOUR%s:WAV?'%(str(slot))
            self.meter.write(msg)
            reading = self.rd()
            print reading
            return float(reading)
        except self.meter.error:
            return None
          
    def get_pow(self,slot):
	""" Get the current power reading """
        try:
            msg = ':READ%s:POW?'%(str(slot))
            self.meter.write(msg)
            done = False
            while not done:
                try:
                    reading = self.rd()
                    done = True
                    return float(reading)
                except:
                    pass
        except:
            print 'problem reading power'
            print reading
    
    def get_avg_time(self,slot):
        """ Get the averaging time """
        try:
            msg = ':SENS%s:POW:ATIM?'%(str(slot))
            self.meter.write(msg)
            reading = self.rd()
            return float(reading)
        except:
            print 'problem reading averaging time'

    def set_avg_time(self,slot,time):
        """ Sets the averaging time in seconds- built in get_avg_time """
        try:
            msg = 'SENS%s:POW:ATIM %fS' %(str(slot),float(time))
            self.meter.write(msg)
            msg = 'SENS%s:POW:ATIM?' %(str(slot))
            self.meter.write(msg)
            reading = self.rd()
            return float(reading)
        except:
            print 'problem setting or reading averaging time'

    def pm_zero_offset(self,slot):
        """ zero the range """ 
        msg = 'SENS:CORR:COLL:ZERO:ALL'
        self.meter.write(msg)
        time.sleep(10)
        try:
            msg = 'SENS%s:CORR:COLL:ZERO?' %(slot)
            self.meter.write(msg)
            time.sleep(0.1) # wait for query
            reading = self.rd()
            return reading
        except:
            print 'problem reading offset'
	
    def pm_set_range(self,slot,range):
        """ set the PM range in dB """
        try:
            msg = 'SENS%s:POW:RANG -%sDBM' %(slot,range)
            self.meter.write(msg)
            time.sleep(0.1) # wait after sending command
            done = False
            while not done:
                try:
                    msg = 'SENS%s:POW:RANG?' %(slot)
                    self.meter.write(msg)
                    time.sleep(0.1) # wait for query
                    reading = self.rd()
                    done = True
                    return float(reading)
                except:
                    print 'problem reading range'
                    pass
        except:
            print 'problem setting range'

    def pm_set_Rauto(self,slot,setting):
        """ set the PM range to auto """
        try:
            msg = 'SENS%s:POW:RANG:AUTO %f' %(slot,setting)
            self.meter.write(msg)
            time.sleep(0.1)
        except:
            print 'problem setting range to auto: must enter a 0 or 1 to set the state of the auto function'

    def pm_is_Rauto(self,slot):
        """ query if PM is in auto range """
        try:
            msg = 'SENS%s:POW:RANG:AUTO?' %(slot)
            self.meter.write(msg)
            reading = self.rd()
            return reading
        except:
            print 'problem querying state of autoranging'
                
    def pm_set_cont_mode(self,slot,setting):
        """ sets whether the PM is continuous """
        done = False
        attempt = 0
        while not done:
            try:
                msg = 'INIT%s:CONT %s' %(slot,setting)
                self.meter.write(msg)
                msg = 'INIT%s:CONT?' %(slot)
                self.meter.write(msg)
                reading = self.rd()
                return reading
            except:
                if attempt == 10:
                    break
                    print 'problem setting the continous mode: must enter a 0 or 1 to set the state'
                else:
        	    attempt += 1
                    pass

### DMM specific commands ###        
    def reset(self):
        """Reset the DMM and it's registers."""
        self.meter.write('*RST')
        self.meter.write('*CLS')
 
    def close(self):
        """End communication with the DMM"""
        self.meter.close()
    
    def info_slot(self,slot):
        #get information on device in slot
        msg = 'SLOT%s:IDN?'%(str(slot))
        self.meter.write(msg)
        reading = self.rd()
        return str(reading)
        
    def info(self):
        #get information on module container
        msg = '*IDN?'
        self.meter.write(msg)
        reading = self.rd()
        return str(reading)
        
    def logdata_query(self,slot,setting):
        """ set the PM range to auto """
        msg = 'SENS%s:FUNC:STAT?'
        self.meter.write(msg)
        reading = self.rd()
        return str(reading)

if (__name__ == '__main__'):
    dev1 = dev('dev10')
    a = dev1.get_lambda(2)
    print '%.2e'%a
    dev1.close()
