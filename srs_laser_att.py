# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:20:02 2014

@author: allen
"""
###### IMPORTS ######
from laser_control import laser_control
import SIM900serial # revise this
import time
from usbenumerator import USBEnumerator
import os
from date_time import date_time
import math

###### DEFINE VALUES ######

measureTime = 2 # in seconds
sleepTime = 5  # in seconds
atten = 27


###### FILE PATHS ######
filePath = 'bias_curves'
if not os.path.exists(filePath):
    os.makedirs(filePath)
Fname=raw_input('detector number: ')

fEfficiency = open(os.path.join(filePath, date_time('dateTime') +'_'+ Fname + '_efficiency.txt'), 'w')

###### DETECT AND CONNECT EXTERNAL DEVICES ######
devices = {'Prologix':{'vendor':'0403','product':'6001'},
       'GenericSerial':{'vendor':'067b','product':'2303'}}
e = USBEnumerator(devices)
sim = SIM900serial.device(e.devices['GenericSerial']['port'])
sPort=e.devices['Prologix']['port']
laserCtrl = laser_control(sPort)


fiberARcorrection=1.036
defLossCorrection = 0.0

photonEnergy=6.62606957e-34*3e8/(1550e-9)


lossCorrection=raw_input('Fiber loss (default = %s dB): ' %defLossCorrection)
if lossCorrection == '':
    lossCorrection = defLossCorrection
else:
    lossCorrection = float(lossCorrection)
    


laserCtrl.set_attenuation_all(0) # sets new power
time.sleep(sleepTime) # waits for power change to settle
Aoff = laserCtrl.read_power()
time.sleep(measureTime) # waits for an averaged  reading to finish
laserCtrl.set_attenuation(atten,0) # sets new power
time.sleep(sleepTime) # waits for power change to settle
A1 = laserCtrl.read_power()
time.sleep(measureTime) # waits for an averaged  reading to finish
laserCtrl.set_attenuation(0,0) # sets new power
laserCtrl.set_attenuation(atten,1) # sets new power
time.sleep(sleepTime) # waits for power change to settle
A2 = laserCtrl.read_power()
time.sleep(measureTime) # waits for an averaged  reading to finish
laserCtrl.set_attenuation(0,1) # sets new power
laserCtrl.set_attenuation(atten,2) # sets new power
time.sleep(sleepTime) # waits for power change to settle
A3 = laserCtrl.read_power()
time.sleep(measureTime) # waits for an averaged  reading to finish
laserCtrl.set_attenuation_all(atten) # sets new power

msg= ''

msg += 'Power @ 0 db %s\n' %str(Aoff)
msg += 'Power 1 @ %s db %s\n' %(str(atten),str(A1)) 
msg += 'Power 2 @ %s db %s\n' %(str(atten),str(A2)) 
msg += 'Power 3 @ %s db %s\n\n' %(str(atten),str(A3)) 

lossA1=10*math.log10(A1/Aoff)
lossA2=10*math.log10(A2/Aoff)
lossA3=10*math.log10(A3/Aoff)
lossTotal=lossA1+lossA2+lossA3

msg += 'loss attnuator 1 = %s\n' %str(lossA1)
msg += 'loss attnuator 2 = %s\n' %str(lossA2)
msg += 'loss attnuator 3 = %s\n' %str(lossA3)
msg += 'total attenuator loss = %s\n\n' %str(lossTotal)

inputPower = Aoff*pow(10,lossTotal/10)
inputPowerAR = inputPower*fiberARcorrection
inputPowerFiberLoss = inputPowerAR*pow(10,-lossCorrection/10)

inputPhotonNumber=inputPower/photonEnergy
inputPhotonNumberAR=inputPowerAR/photonEnergy
inputPhotonNumberFiberLoss=inputPowerFiberLoss/photonEnergy

msg += 'input power = %s\n' %str(inputPower)
msg += 'input power AR corrected = %s\n' %str(inputPowerAR)
msg += 'input power AR + fiber loss correted = %s\n' %str(inputPowerFiberLoss)

msg += 'input photons = %s\n' %str(inputPhotonNumber)
msg += 'input photons AR corrected = %s\n' %str(inputPhotonNumberAR)
msg += 'input photons AR + fiber loss correted = %s\n' %str(inputPhotonNumberFiberLoss)


fEfficiency.write(msg)
fEfficiency.flush()
print msg

fEfficiency.close()
