import SIM900serial # revise this
import time
import numpy
from usbenumerator import USBEnumerator
import os
from date_time import date_time

class temp_log:
    def __init__(self, interval=1, average = 5, printOutput=True):
        #open the SIM900 device
        devices = {'Prologix':{'vendor':'0403','product':'6001'},
               'GenericSerial':{'vendor':'067b','product':'2303'}}
        e = USBEnumerator(devices)
        self.sim = SIM900serial.device(e.devices['GenericSerial']['port'])
        if printOutput:
            print 'SIM900 on port '+e.devices['GenericSerial']['port']
            print e.devices
            #select calibration cures for the thermometers to use
        self.select_calibration('sim921',5)
        self.select_calibration('sim922',7)
        self.select_calibration('sim922',8)
        self.interval=interval
        self.average=average
        self.blankMatrix = numpy.zeros(7*self.average).reshape((7,self.average))
        self.logging=False
	self.logRaw=False
        
    def select_calibration(self, device, port):
        self.sim.conn(port)
        if device == 'sim922':
            self.sim.write('CURV 0, 1')
        elif device == 'sim921':
            self.sim.write('CURV 1')
        self.sim.write('xyz')

    def open_log_file(self,fileName='test',logRaw=False,writeMethod='a'):
        filePath = 'temperature_logs'
        if not os.path.exists(filePath):
            os.makedirs(filePath)
        self.fileNameChase = 'templog_chase.txt'     #the regen reads from this file
        self.logFile = open(os.path.join(filePath, date_time('date') +'_templog.txt'), 'a')
        if self.logRaw:
            self.logRawFile = open(os.path.join(filePath, date_time('date') +'_templog_raw.txt'), 'a')    
            
    def save_chase(self, data):
        logFile_chase = open(self.fileNameChase,'w')
        logFile_chase.write(data)
        logFile_chase.close()

    def save_raw(self, data):
        if self.logRaw:
            self.logRawFile.write(data)
            self.logRawFile.flush()
        
    def save_avg(self, data):
        self.logFile.write(data)
        self.logFile.flush()
        
    def close_files(self):
        self.logFile.close()
        if self.logRaw:
            self.logRawFile.close()
            
    def read_temp(self,port, meter=''):
        self.sim.conn(port)
        self.sim.write('TVAL? %s' %str(meter))
        time.sleep(0.1)
        msg = self.sim.read()
        msg = msg.replace(',','\t').replace('\n','\t').replace('\r','').replace(' ','')
        self.sim.write('xyz')        
        return msg

    def log(self):
        self.logging=True
	self.open_log_file()
        avgMatrix= self.blankMatrix
        ITERavgLoop = 0
        while self.logging:
            timeString = '%.2f\t'%time.time()
            tempArray=[]
            rawData=""
            rawData=self.read_temp(8,1)
            rawData+=self.read_temp(8,2)
            rawData+=self.read_temp(7,0)
            rawData+=self.read_temp(5)
            ITERrawLoop=0
            for temp in rawData.split():
                try:
                    tempArray.append(float(temp))
                    if float(temp) <400:
                        #n=n+t+'\t'
                        avgMatrix[ITERrawLoop][ITERavgLoop]= float(temp)
                    else:
                        #n=n+'\t'
                        avgMatrix[ITERrawLoop][ITERavgLoop]=0       
                except ValueError:
                    pass 
                ITERrawLoop+=1
            time.sleep(self.interval-0.4)
            rawData=timeString + rawData 
            self.save_raw(rawData)
            if ITERavgLoop == (self.average-1):
                avgData=''
                ITERavgLoop = -1
                avgMatrixSum=avgMatrix.sum(1)
                avgMatrixCount=(avgMatrix !=0).sum(1)
                for z in range(0,7):
                    if avgMatrixCount[z] == 0:
                        avgMatrixCount[z] = 1
                avgMatrix=avgMatrixSum/avgMatrixCount
                for x in range(0,7):
                    avgData+= '%f'%float(avgMatrix[x]) + '\t'
                avgData=timeString + avgData 
                self.curentData=avgData
                self.save_avg(avgData)
                self.save_chase(avgData)
                avgMatrix =self.blankMatrix
            ITERavgLoop += 1
        
if __name__ == '__main__':
    templog=temp_log()   #initialise system
    templog.log()
    templog.close_files()
