import serial

RQS = (1<<11)
SRQ = (1<<12)
TIMO = (1<<14)


class Gpib:
    def __init__(self,addr,serialport):
        self.port = serialport
        if self.port.isOpen()==False:
            self.port.open()
        self.addr = addr;

    def address(self):        #set GPIB address of device 
        self.port.write('++addr %d\n'%self.addr)
    def write(self,str):    #Write someting to the GPIB, all commands use this
        self.address
        self.port.write('%s\n'%str)
    def read(self,len=512): #Read return stream from GPIB
        self.address
        self.port.write('++read\n')
        self.res = self.port.read(len)
        return self.res
    def clear(self):
        self.address
        self.port.write('++clr\n')
    def rsp(self):
        self.address
        self.port.write('++spoll\n')
        self.spb = integer(self.port.read(100));
        return self.spb
    def trigger(self):
        self.address
        self.port.write('++trg\n')
    def loc(self):
        self.address
        self.port.write('++loc\n')
############
    def auto(self, state=""): #automativcally makes device READ or LISTEN after commands are given
         #sets device mode; 0- LISTEN, 1 - TALK, no input - returns current state        
        self.address
        self.port.mode(1) #must be in CONTROLLER mode
        self.port.write('++auto \r\n' %state)
        if state == "":
            self.res = self.port.read(len)
            return self.res
            
    def mode(self, state=""): 
        #sets device mode; 1- CONTROLLER, 0 - DEVICE, no input - returns current state
        self.address
        self.port.write('++mode %r\n' %state)
        if state == "":
            msg = self.read()
            return self.res
            
#    def (self):
#        self.address
#        self.port.write('++\n')
#        
#    def (self):
#        self.address
#        self.port.write('++\n')
#        
#    def (self):
#        self.address
#        self.port.write('++\n')