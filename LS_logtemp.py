# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 13:58:57 2013

@author: qittlab
"""

import Gpib_prologix_nt_ver2 as Gpib
import serial
import time
import os
import sys
import numpy as np
import datetime
from date_time import date_time

serialport = serial.Serial('/dev/ttyUSB0',9600,timeout=0.5)

ls = Gpib.Gpib(12,serialport)
ls.write('*IDN?')

msg = ls.read(100)
print msg

list = ['C2','C3','D2','D3','D4','D5','A'];

filePath = 'temperature_logs' # selects the folder to save log files in
f = open(os.path.join(filePath, date_time('date') + 'templogdata.txt'),'a') # timestamp the output file
# init variables as zero for averaging

while True:
  msglog = '%.2f\t'%time.time()
  for ch in list:
#    if (ch[0]=='A')or (ch=='D5'):
#      msgout = 'SRDG? %s'%ch;
#    else:
    if True:
      msgout = 'KRDG? %s'%ch;
    ls.write(msgout)
    valstr = ls.read(100)
#    print repr(valstr)
    msglog = msglog + valstr.strip() + '\t'
#  ls.write('RELAYST? 1')
  valstr = ls.read(100)
  msglog = msglog + valstr.strip() + '\t'
#  ls.write('INTYPE? D1')
  valstr = ls.read(100)
  valstr = valstr.split(',')
  valstr = valstr[0]
  msglog = msglog + valstr.strip() + '\t'
  print msglog
  g = open('templog_chase.txt','w')
  g.write(msglog)
  g.flush()
  g.close()
  f.write(msglog+'\n')
  f.flush()
  time.sleep(5)
ls.loc()
