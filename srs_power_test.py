# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:20:02 2014

@author: allen
"""
###### IMPORTS ######
from laser_control import laser_control
from usb31xx import usb31xx
from sr400 import sr400
import SIM900serial # revise this
import time
from usbenumerator import USBEnumerator
import os
from date_time import date_time
import math

###### DEFINE VALUES ######

measureTime = 1 # in seconds
sleepTime = 5  # in seconds
filePath = 'nonlinearity' # folder for saving logs
atten = 27

fiberARcorrection=1.036
switchCorrection = 0.8715
defLossCorrection = 0.0

photonEnergy=6.62606957e-34*3e8/(1550e-9)

###### DETECT AND CONNECT EXTERNAL DEVICES ######
devices = {'Prologix':{'vendor':'0403','product':'6001'},
       'GenericSerial':{'vendor':'067b','product':'2303'}}
e = USBEnumerator(devices)
sPort=e.devices['Prologix']['port']
laserCtrl = laser_control(sPort)



##########################
###### MAIN PROGRAM ######
##########################
Aoff=0.0
A11=0.0
A21=0.0
A31=0.0
lossA11=0.0
lossA21=0.0
lossA31=0.0
Tloss=0.0


for n in range(0,10):
    laserCtrl.set_attenuation_all(0) # sets new power
    time.sleep(sleepTime) # waits for power change to settle
    Aoff = laserCtrl.read_power()
    time.sleep(measureTime) # waits for an averaged  reading to finish
    laserCtrl.set_attenuation(atten,0) # sets new power
    time.sleep(sleepTime) # waits for power change to settle
    A1 = laserCtrl.read_power()
    A11+=A1
    time.sleep(measureTime) # waits for an averaged  reading to finish
    laserCtrl.set_attenuation(0,0) # sets new power
    laserCtrl.set_attenuation(atten,1) # sets new power
    time.sleep(sleepTime) # waits for power change to settle
    A2 = laserCtrl.read_power()
    A21+=A2
    time.sleep(measureTime) # waits for an averaged  reading to finish
    laserCtrl.set_attenuation(0,1) # sets new power
    laserCtrl.set_attenuation(atten,2) # sets new power
    time.sleep(sleepTime) # waits for power change to settle
    A3 = laserCtrl.read_power()
    A31 +=A3
    time.sleep(measureTime) # waits for an averaged  reading to finish
    laserCtrl.set_attenuation_all(atten) # sets new power
    
    msg= '******%str*****\n'%str(n)
    
    msg += 'Power 0 %s\t' %str(Aoff)
    msg += 'Power 1 %s\t' %str(A1)
    msg += 'Power 2 %s\t' %str(A2)
    msg += 'Power 3 %s\t\n' %str(A3)
    
    lossA1=10*math.log10(A1/Aoff)
    lossA2=10*math.log10(A2/Aoff)
    lossA3=10*math.log10(A3/Aoff)
    lossTotal=lossA1+lossA2+lossA3
    lossA11+=lossA1
    lossA21+=lossA2
    lossA31+=lossA3
    Tloss+=lossTotal
    
    msg += 'loss 1 = %s\t' %str(lossA1)
    msg += 'loss 2 = %s\t' %str(lossA2)
    msg += 'loss 3 = %s\t' %str(lossA3)
    msg += 'total loss = %s\t\n' %str(lossTotal)
    
    
    print msg
print Aoff/10
print A11/10
print A21/10
print A31/10
print lossA11/10
print lossA21/10
print lossA31/10
print Tloss/10
