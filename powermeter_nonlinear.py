# -*- coding: utf-8 -*-
"""
Created on Tue Mar  4 16:13:27 2014

@author: allen
"""

import time
from laser_control import laser_control
from date_time import date_time

laserCtrl=laser_control()
logfile = open(date_time('dateTime')+'_OFPM_NL.txt','w')

print 'Atten (dB) \t off \t A on \t B on \t Both \n'
for z in range(0,61):
    laserCtrl.set_attenuation_all(z)
    laserCtrl.set_switch_port('A',2)
    laserCtrl.set_switch_port('B',2)
    time.sleep(2)
    msgBackground = laserCtrl.read_power()
    laserCtrl.set_switch_port('A',1)
    time.sleep(2)
    msgA = laserCtrl.read_power()
    laserCtrl.set_switch_port('B',1)
    time.sleep(2)
    msgBoth = laserCtrl.read_power()
    laserCtrl.set_switch_port('A',2)
    time.sleep(2)
    msgB = laserCtrl.read_power()
    
    msg= str(z) +'\t'+ str(msgBackground) +'\t'+ str(msgA) +'\t'+ str(msgB) +'\t'+str(msgBoth) +'\n'
    print msg
    logfile.write(msg)
logfile.close()
