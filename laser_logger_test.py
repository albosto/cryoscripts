# -*- coding: utf-8 -*-
"""
Created on Tue Mar  4 17:28:01 2014

@author: allen
"""


import time
from laser_control import laser_control
laserCtrl=laser_control()
done = False
lpowArrayZero=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] 
timeArrayZero=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0] 
lpowArray=lpowArrayZero
timeArray=timeArrayZero
arrayPoint=0
filename = 'laser_log.txt'
logfile = open(filename,'w')
while not done:
    time.sleep(.5)
    reading=True
    while reading:   
        try:
            lpow = laserCtrl.read_power()
            reading = False
        except:
            print 'ERROR!!'
    lpowArray[arrayPoint]=float(lpow)
    timeArray[arrayPoint]=float(time.time())
    arrayPoint+=1
    if arrayPoint == 10:
        arrayPoint=0
        for x in range (0,10):
            msg= str(timeArray[x])+'\t'+str(lpowArray[x])+'\n'
            print msg
	    logfile.write(msg)
        lpowArray=lpowArrayZero
        timeArray=timeArrayZero
