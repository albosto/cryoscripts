import serial

RQS = (1<<11)
SRQ = (1<<12)
TIMO = (1<<14)


class Gpib:
    def __init__(self,addr,serialport):
        self.port = serialport
        if self.port.isOpen()==False:
            self.port.open()
        self.addr = addr;

    def write(self,str):
          self.port.write('++addr %d\n'%self.addr)
          self.port.write('%s\n'%str)
    
    def read(self,len=512):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++read eoi\n')
        self.res = self.port.read(len)
        return self.res

    def clear(self):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++clr\n')

    def rsp(self):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++spoll\n')
        self.spb = integer(self.port.read(100));
        return self.spb

    def trigger(self):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++trg\n')

    def loc(self):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++loc\n')

    def timeout(self,str):
        self.port.write('++addr %d\n'%self.addr)
        self.port.write('++read_tmo_ms %s\n'%str)
