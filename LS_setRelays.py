# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 13:58:57 2013

@author: qittlab
"""

import Gpib_prologix_nt_ver2 as Gpib
import serial
import time
import os
import sys
import numpy as np
import datetime

serialport = serial.Serial('/dev/ttyUSB2',9600,timeout=0.5)

ls = Gpib.Gpib(12,serialport)
ls.write('*IDN?')
msg = ls.read(100)
print msg
done = False
while not done:
	relay = str(raw_input('Relay port number (1 or 2) :'))
	state = str(raw_input('Port state (0 or 1) :'))
	if relay not in ('1','2'):
		done = True
		break
	msg = 'RELAY %s,%s' %(relay,state)
	print msg
	ls.write(msg)
	time.sleep(1)
	ls.write('RELAYST? 1')
	msg1 = ls.read(100)
	ls.write('RELAYST? 2')
	msg2 = ls.read(100)
	print msg1 + msg2
ls.loc()
