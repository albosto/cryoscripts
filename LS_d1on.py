# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 13:58:57 2013

@author: qittlab
"""

import Gpib_prologix_nt_ver2 as Gpib
import serial
import time
import os
import sys
import numpy as np
import datetime

serialport = serial.Serial('/dev/ttyUSB0',9600,timeout=0.5)

ls = Gpib.Gpib(12,serialport)
ls.write('*IDN?')
msg = ls.read(100)
print msg
ls.write('INTYPE D1,2,1,6,1,3')
time.sleep(1)
ls.write('INTYPE? D1')
msg = ls.read(100)
print msg
ls.loc()
