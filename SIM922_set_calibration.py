#!/usr/bin/env python

import sys,os
import struct
import SIM900serial # revise this
import time
import string
from usbenumerator import USBEnumerator
import signal
import numpy as np

devices = {'Prologix':{'vendor':'0403','product':'6001'}, 
           'GenericSerial':{'vendor':'067b','product':'2303'}}
e = USBEnumerator(devices)

print 'SIM900 on port '+e.devices['GenericSerial']['port']
#print 'SIM900 on port '+e.devices['Prologix']['port']
sim = SIM900serial.device(e.devices['GenericSerial']['port'])
#sim = SIM900serial.device(e.devices['Prologix']['port'])

slot = 7
c =4
sim.conn(slot)

#d = np.loadtxt('DT670.txt')
#sim.write('CINI %d,0,DT670'%c)
d = np.loadtxt('FBdiode.txt',skiprows=1)
#sim.write('CINI %d,0,FBdiode'%c)
#d = np.loadtxt('Diodecal3.txt',skiprows=2)
#sim.write('CINI %d,0,Diodecal3'%c)
sim.write('CINI %d,0,FBdiode'%c)
#sim.write('CINI? %d'%c)
if True:
  for idx in (range(len(d[:,0]))):
    f = d[idx,1];
    g = d[idx,0];
    s = 'CAPT %d,%.3f,%.3f' % (c,f,g)
    sim.write(s)
    print s
    time.sleep(1)
if False:
  for val in range(129):
    s = 'CAPT? %d,%d' %(c,val)
    sim.write(s)
    time.sleep(1)
    msg = sim.read()
    print '%d'%val + msg
if False:
    sim.write('CINI? %d' %c)   
    time.sleep(1)
    msg = sim.read()
    time.sleep(1)
    print msg

sim.write('CURV %d,1'%c)
sim.write('CINI? %d'%c)
msg = sim.read()
print msg
sim.write('xyz')
sim.meter.close()

#sim.write('VOLT? %d'%c)
#time.sleep(1)
#msg = sim.read()
#time.sleep(1)
#sim.write('xyz')
#sim.meter.close()
#print msg

print 'Goodbye'



