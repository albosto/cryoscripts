# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:20:02 2014

@author: allen
"""

import serial
import Gpib_prologix_nt_ver2 as Gpib
class sr400():
    def __init__(self,addr,serialport='/dev/ttyUSB0'):
        s = serial.Serial(serialport,9600,timeout=1);
        self.gpib = Gpib.Gpib(addr,s)
#################################
#   MODE                        #
#################################
    def counter_mode(self, value=''):
        self.gpib.write("CM %r" %value)
        if value == '':
            msg = self.gpib.read()
            return msg
            
    def counter_input(self, counter, input = ''):
        if counter not in (0,1,2) or input not in ('',0,1,2,3):
            return 'error:invalid input'
        if input == "":
            self.gpib.write("CI %r" %(counter,input))
            msg = self.gpib.read()
            return msg
        else:
            self.gpib.write("CI %r,%r" %(counter,input))
            
    def counter_presets(self, counter, value=""):
        if counter not in (1,2) or input > 9E11 or input < 1:
            return 'error:invalid input'
        self.gpib.write("CP %r,%r" %value)
        if value == "":
            msg = self.gpib.read()
            return msg
            
    def number_periods(self, value=''):
        self.gpib.write("NP %r" %value)
        if value == '':
            msg = self.gpib.read()
            return msg
            
    def scan_position(self):
        self.gpib.write("NN")
        msg = self.gpib.read()
        return msg
#################################
#   LEVELS                      #
#################################

#################################
#   GATES                       #
#################################

#################################
#   FRONT PANEL                 #
#################################
    def counter_start(self):
        self.gpib.write("CS")
        
    def counter_stop(self):
        self.gpib.write("CH")
        
    def counter_reset(self):
        self.gpib.write("CR")
        
#################################
#   INTERFACE                   #
#################################
#################################
#   STORE/RECALL                #
#################################


#################################
#   POLLED DATA                 #
#################################
    def getcounts_a(self):
        self.gpib.write("QA")
        msg = self.gpib.read()
        return msg
        
    def getcounts_b(self):
        self.gpib.write("QB")
        msg = self.gpib.read()
        return msg 
#################################
#   CONTINUOUS DATA             #
#################################

#################################
#   EXAMINE DATA                #
#################################

#################################
#   STATUS BYTE                 #
#################################

#################################
#   SECONDARY STATUS BYTE       #
#################################

import time
counter=sr400(23,'/dev/ttyUSB0')
#counter.gpib.reset()
counter.gpib.write('++addr')
mode = counter.gpib.read()
print mode
print 'done'